# RU-Website-Prod-Backup

This repository is responsible for maintaining the main assets of the [RU Production Website](https://www.racing-unleashed.com/). 

## Current Procedure

- Every Release this repo will be updated with what was existing on the production site, as to preserve it's history.
- This is currently being copied over from [Beyond Compare](https://www.scootersoftware.com/download.php) during the deployment process